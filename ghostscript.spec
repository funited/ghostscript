%global _hardened_build 1
# override the default location of documentation or license files
# in 'ghostscript' instead of in 'libgs'
%global _docdir_fmt     %{name}
# download version
%global version_short   %(echo "%{version}" | tr -d '.')
# Obtain the location of Google Droid fonts directory
%global google_droid_fontpath %%(dirname $(fc-list : file | grep "DroidSansFallback"))

Name:             ghostscript
Version:          9.52
Release:          5
Summary:          An interpreter for PostScript and PDF files
License:          AGPLv3+
URL:              https://ghostscript.com/
Source0:          https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs%{version_short}/ghostscript-%{version}.tar.xz

Patch0:  ghostscript-9.23-100-run-dvipdf-securely.patch
Patch1:  jbig2dec-Handle-under-overflow-detection-and-messaging-better.patch
Patch2:  jbig2dec-Add-overflow-detection-for-IAID-context-size.patch
Patch3:  jbig2dec-Avoid-warning-by-copying-bytes-instead-of-characters.patch
Patch4:  Bug-701721-jbig2dec-Fix-under-overflow-handling-in-arithmetic-integer-decoder.patch
Patch5:  jbig2dec-Always-use-uint32_t-when-counting-pages.patch
Patch6:  PDF-interpreter-swallow-errors-reading-ICC-profiles-and-continue.patch
Patch7:  jbig2dec-Fix-two-overlooked-warnings.patch
Patch8:  jbig2dec-Use-correct-define-for-maxium-value-of-type.patch
Patch9:  Fix-Bug-702181-SEGV-when-BufferSpace-is-at-or-above-K-alloc-limit.patch
Patch10: Fix-bug-702182-VMerror-due-to-leaks-in-pattern-cache-due-to-locking.patch
Patch11: Bug-702196-Fix-incorrect-detection-of-thin-lines-while-stroking.patch
Patch12: Fix-Bug-702177-VMerrors-with-some-BufferSpace-andor-K-limits.patch
Patch13: Fix-infinite-loop-in-PDF-interpreter-pagespotcolors-procedure.patch
Patch14: Coverity-94826-Add-missing-offset-to-buffer-size-used-for-clist-cmd.patch
Patch15: jbig2dec-Plug-leak-of-image-upon-error.patch
Patch16: jbig2dec-Adjust-number-of-bytes-consumed-by-MMR-decoder.patch
Patch17: jbig2dec-Initiate-variable-before-avoids-using-uninited-data-during-cleanup.patch
Patch18: PostScript-interpreter-don-t-leave-A85Decode-pdf_rules-uninitialised.patch
Patch19: Bug-702322-fix-uninitalized-data-reads.patch
Patch20: Bug-702320-Valgrind-complains-about-UMR.patch
Patch21: Bug-702335-jbig2dec-Refill-input-buffer-upon-failure-to-parse-segment-header.patch
Patch22: Bug-697545-Prevent-memory-leak-in-gx-path-assign-free.patch
Patch23: Bug-697545-Prevent-numerous-memory-leaks.patch
Patch24: lgtmcom-tweak-Make-it-clear-that-something-isn-t-a-typo.patch
Patch25: Bug-702582-CVE-2020-15900-Memory-Corruption-in-Ghost.patch
Patch26: oss-fuzz-22182-validate-glyph-offset-length-values.patch
Patch27: oss-fuzz-23637-Fix-error-code-confusion.patch
Patch28: oss-fuzz-23946-Move-buffer-bounds-check-to-before-us.patch

BuildRequires:    automake gcc
BuildRequires:    adobe-mappings-cmap-devel adobe-mappings-pdf-devel
BuildRequires:    google-droid-sans-fonts urw-base35-fonts-devel
BuildRequires:    cups-devel dbus-devel fontconfig-devel
BuildRequires:    lcms2-devel libidn-devel libijs-devel libjpeg-turbo-devel
BuildRequires:    libpng-devel libpaper-devel libtiff-devel openjpeg2-devel
BuildRequires:    zlib-devel gtk3-devel libXt-devel
BuildRequires:    jbig2dec-devel >= 0.16

Requires:         adobe-mappings-cmap
Requires:         adobe-mappings-cmap-lang
Requires:         adobe-mappings-pdf
Requires:         google-droid-sans-fonts
Requires:         urw-base35-fonts

Obsoletes:      %{name}-doc < %{version}-%{release}
Obsoletes:      %{name}-x11 < %{version}-%{release}
Obsoletes:      %{name}-gtk < %{version}-%{release}
Obsoletes:      %{name}-tools-printing < %{version}-%{release}
Obsoletes:      %{name}-tools-fonts < %{version}-%{release}
Obsoletes:      libgs < %{version}-%{release}
Provides:       %{name}-doc
Provides:       %{name}-x11
Provides:       %{name}-gtk
Provides:       %{name}-tools-printing
Provides:       %{name}-tools-fonts
Provides:       libgs
Provides:       %{name}-core

%description
Ghostscript is an interpreter for PostScript™ and Portable Document Format (PDF) files.
Ghostscript consists of a PostScript interpreter layer, and a graphics library.

%package devel
Summary:          Development files for Ghostscript's library
Requires:         %{name} = %{version}-%{release}

Obsoletes:        libgs-devel < %{version}-%{release}
Provides:         libgs-devel

%description devel
This package contains development files for %{name}.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       man info
Requires:       %{name} = %{version}-%{release}

Obsoletes:      %{name}-doc < %{version}-%{release}
Provides:       %{name}-doc

%description help
Man pages and other related documents for %{name}.

%package tools-dvipdf
Summary:          Ghostscript's 'dvipdf' utility
Requires:         %{name} = %{version}-%{release}
Requires:         texlive-dvips

%description tools-dvipdf
This package provides the utility 'dvipdf' for converting of TeX DVI files into
PDF files using Ghostscript and dvips

%prep
%autosetup -n %{name}-%{version} -p1

# Libraries that we already have packaged(see Build Requirements):
rm -rf cups/libs freetype ijs jbig2dec jpeg lcms2* libpng openjpeg tiff zlib
rm -rf windows

%build
%configure --enable-dynamic --disable-compile-inits --without-versioned-path \
           --with-fontpath="%{urw_base35_fontpath}:%{google_droid_fontpath}:%{_datadir}/%{name}/conf.d/"
%make_build so

%install
# to install necessary files without 'make_install'
make DESTDIR=%{buildroot} soinstall

# rename to 'gs' binary.
mv -f %{buildroot}%{_bindir}/{gsc,gs}

# remove files
rm -f %{buildroot}%{_bindir}/{lprsetup.sh,unix-lpr.sh}
rm -f %{buildroot}%{_docdir}/%{name}/{AUTHORS,COPYING,*.tex,*.hlp,*.txt}
rm -f %{buildroot}%{_datadir}/%{name}/doc

# move some files into html/
install -m 0755 -d %{buildroot}%{_docdir}/%{name}/html
cp doc/gsdoc.el %{buildroot}%{_docdir}/%{name}/
mv -f %{buildroot}%{_docdir}/%{name}/{*.htm*,*.el,html}

# create symlink
ln -s %{_bindir}/gs %{buildroot}%{_bindir}/ghostscript
ln -s %{_mandir}/man1/gs.1 %{buildroot}%{_mandir}/man1/ghostscript.1

# use the symlinks where possible.
ln -fs %{google_droid_fontpath}/DroidSansFallback.ttf %{buildroot}%{_datadir}/%{name}/Resource/CIDFSubst/DroidSansFallback.ttf

for font in $(basename --multiple %{buildroot}%{_datadir}/%{name}/Resource/Font/*); do
  ln -fs %{urw_base35_fontpath}/${font}.t1 %{buildroot}%{_datadir}/%{name}/Resource/Font/${font}
done

# create symlink for each of the CMap files in Ghostscript's Resources/CMap folder.
for file in $(basename --multiple %{buildroot}%{_datadir}/%{name}/Resource/CMap/*); do
  find %{adobe_mappings_rootpath} -type f -name ${file} -exec ln -fs {} %{buildroot}%{_datadir}/%{name}/Resource/CMap/${file} \;
done

install -m 0755 -d %{buildroot}%{_datadir}/%{name}/conf.d/

%check
%make_build check

%pre

%preun

%post

%postun

%files
%defattr(-,root,root)
%license LICENSE doc/COPYING
%{_datadir}/%{name}/
%dir %{_datadir}/%{name}/conf.d/
%{_bindir}/gs
%{_bindir}/gsnd
%{_bindir}/ghostscript
%{_bindir}/eps2*
%{_bindir}/pdf2*
%{_bindir}/ps2*
%{_bindir}/gsx
%{_bindir}/gsbj
%{_bindir}/gsdj
%{_bindir}/gsdj500
%{_bindir}/gslj
%{_bindir}/gslp
%{_bindir}/pphs
%{_bindir}/pf2afm
%{_bindir}/pfbtopfa
%{_bindir}/printafm
%{_libdir}/libgs.so.*
%{_libdir}/%{name}/

%files devel
%{_libdir}/libgs.so
%{_includedir}/%{name}/

%files help
%{_mandir}/man1/*
%lang(de) %{_mandir}/de/man1/*
%doc %{_docdir}/%{name}/

%files tools-dvipdf
%{_bindir}/dvipdf

%changelog
* Mon Apr 19 2021 panxiaohe <panxiaohe@huawei.com> - 9.52-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:use make macros to run check in parallel

* Sat Oct 31 2020 Liquor <lirui130@huawei.com> - 9.52-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix problems detected by oss-fuzz test

* Thu Sep 10 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 9.52-3
- Type:bugfix
- ID:CVE-2020-15900
- SUG:NA
- DESC:fix CVE-2020-15900

* Thu Sep 3 2020 wangchen <wangchen137@huawei.com> - 9.52-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Sync some patches from community

* Sun Jun 28 2020 wangchen <wangchen137@huawei.com> - 9.52-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update ghostscript to 9.52

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 9.27-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix autosetup patch

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 9.27-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add run dvipdf securely

* Tue Jan 7 2020 chengquan<chengquan3@huawei.com> - 9.27-6
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2019-14869

* Tue Jan 7 2020 chengquan<chengquan3@huawei.com> - 9.27-5
- Type:CVE
- ID:NA
- SUG:NA
- DESC:remove useless patch

* Fri Jan 3 2020 wangxiao<wangxiao65@huawei.com> - 9.27-4
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2019-14811 CVE-2019-14812 CVE-2019-14813 CVE-2019-14817

* Mon Sep 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 9.27-3
- fix CVE-2019-10216 and modify requires

* Mon Sep 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 9.27-2
- Add subpackage tools-dvipdf

* Thu Sep 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 9.27-1
- Package init
